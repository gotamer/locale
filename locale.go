// Internationalization and localization.
package locale

import (
	"errors"
	// "os"
	"strings"
)

const (
	//PS             = string(os.PathSeparator)
	default_locale = "en_US"
	//zend           = "/usr/share/php/Zend/Locale/Data"
)

type ttt [2]string // Territory Type
var (
	// BaseDir       string
	DefaultLocale string
	dataLocale    = [400]string{"aa_DJ", "aa_ER", "aa_ET", "aa", "af_NA", "af_ZA", "af", "ak_GH", "ak", "am_ET", "am", "ar_AE", "ar_BH", "ar_DZ", "ar_EG", "ar_IQ", "ar_JO", "ar_KW", "ar_LB", "ar_LY", "ar_MA", "ar_OM", "ar_QA", "ar_SA", "ar_SD", "ar_SY", "ar_TN", "ar_YE", "ar", "as_IN", "as", "az_AZ", "az", "be_BY", "be", "bg_BG", "bg", "bn_BD", "bn_IN", "bn", "bo_CN", "bo_IN", "bo", "bs_BA", "bs", "byn_ER", "byn", "ca_ES", "ca", "cch_NG", "cch", "cop", "cs_CZ", "cs", "cy_GB", "cy", "da_DK", "da", "de_AT", "de_BE", "de_CH", "de_DE", "de_LI", "de_LU", "de", "dv_MV", "dv", "dz_BT", "dz", "ee_GH", "ee_TG", "ee", "el_CY", "el_GR", "el", "en_AS", "en_AU", "en_BE", "en_BW", "en_BZ", "en_CA", "en_GB", "en_GU", "en_HK", "en_IE", "en_IN", "en_JM", "en_MH", "en_MP", "en_MT", "en_NA", "en_NZ", "en_PH", "en_PK", "en_SG", "en_TT", "en_UM", "en_US", "en_VI", "en_ZA", "en_ZW", "en", "eo", "es_AR", "es_BO", "es_CL", "es_CO", "es_CR", "es_DO", "es_EC", "es_ES", "es_GT", "es_HN", "es_MX", "es_NI", "es_PA", "es_PE", "es_PR", "es_PY", "es_SV", "es_US", "es_UY", "es_VE", "es", "et_EE", "et", "eu_ES", "eu", "fa_AF", "fa_IR", "fa", "fi_FI", "fi", "fil_PH", "fil", "fo_FO", "fo", "fr_BE", "fr_CA", "fr_CH", "fr_FR", "fr_LU", "fr_MC", "fr_SN", "fr", "fur_IT", "fur", "ga_IE", "ga", "gaa_GH", "gaa", "gez_ER", "gez_ET", "gez", "gl_ES", "gl", "gsw_CH", "gsw", "gu_IN", "gu", "gv_GB", "gv", "ha_GH", "ha_NE", "ha_NG", "ha_SD", "ha", "haw_US", "haw", "he_IL", "he", "hi_IN", "hi", "hr_HR", "hr", "hu_HU", "hu", "hy_AM", "hy", "ia", "id_ID", "id", "ig_NG", "ig", "ii_CN", "ii", "in", "is_IS", "is", "it_CH", "it_IT", "it", "iu", "iw", "ja_JP", "ja", "ka_GE", "ka", "kaj_NG", "kaj", "kam_KE", "kam", "kcg_NG", "kcg", "kfo_CI", "kfo", "kk_KZ", "kk", "kl_GL", "kl", "km_KH", "km", "kn_IN", "kn", "ko_KR", "ko", "kok_IN", "kok", "kpe_GN", "kpe_LR", "kpe", "ku_IQ", "ku_IR", "ku_SY", "ku_TR", "ku", "kw_GB", "kw", "ky_KG", "ky", "ln_CD", "ln_CG", "ln", "lo_LA", "lo", "lt_LT", "lt", "lv_LV", "lv", "mk_MK", "mk", "ml_IN", "ml", "mn_CN", "mn_MN", "mn", "mo", "mr_IN", "mr", "ms_BN", "ms_MY", "ms", "mt_MT", "mt", "my_MM", "my", "nb_NO", "nb", "nds_DE", "nds", "ne_IN", "ne_NP", "ne", "nl_BE", "nl_NL", "nl", "nn_NO", "nn", "no", "nr_ZA", "nr", "nso_ZA", "nso", "ny_MW", "ny", "oc_FR", "oc", "om_ET", "om_KE", "om", "or_IN", "or", "pa_IN", "pa_PK", "pa", "pl_PL", "pl", "ps_AF", "ps", "pt_BR", "pt_PT", "pt", "ro_MD", "ro_RO", "ro", "ru_RU", "ru_UA", "ru", "rw_RW", "rw", "sa_IN", "sa", "se_FI", "se_NO", "se", "sh_BA", "sh_CS", "sh_YU", "sh", "si_LK", "si", "sid_ET", "sid", "sk_SK", "sk", "sl_SI", "sl", "so_DJ", "so_ET", "so_KE", "so_SO", "so", "sq_AL", "sq", "sr_BA", "sr_CS", "sr_ME", "sr_RS", "sr_YU", "sr", "ss_SZ", "ss_ZA", "ss", "st_LS", "st_ZA", "st", "sv_FI", "sv_SE", "sv", "sw_KE", "sw_TZ", "sw", "syr_SY", "syr", "ta_IN", "ta", "te_IN", "te", "tg_TJ", "tg", "th_TH", "th", "ti_ER", "ti_ET", "ti", "tig_ER", "tig", "tl", "tn_ZA", "tn", "to_TO", "to", "tr_TR", "tr", "trv_TW", "trv", "ts_ZA", "ts", "tt_RU", "tt", "ug_CN", "ug", "uk_UA", "uk", "ur_IN", "ur_PK", "ur", "uz_AF", "uz_UZ", "uz", "ve_ZA", "ve", "vi_VN", "vi", "wal_ET", "wal", "wo_SN", "wo", "xh_ZA", "xh", "yo_NG", "yo", "zh_CN", "zh_HK", "zh_MO", "zh_SG", "zh_TW", "zh", "zu_ZA", "zu"}
	dataTerritory = [245]ttt{ttt{"AD", "ca_AD"}, ttt{"AE", "ar_AE"}, ttt{"AF", "fa_AF"}, ttt{"AG", "en_AG"}, ttt{"AI", "en_AI"}, ttt{"AL", "sq_AL"}, ttt{"AM", "hy_AM"}, ttt{"AN", "pap_AN"}, ttt{"AO", "pt_AO"}, ttt{"AQ", "und_AQ"}, ttt{"AR", "es_AR"}, ttt{"AS", "sm_AS"}, ttt{"AT", "de_AT"}, ttt{"AU", "en_AU"}, ttt{"AW", "nl_AW"}, ttt{"AX", "sv_AX"}, ttt{"AZ", "az_Latn_AZ"}, ttt{"BA", "bs_BA"}, ttt{"BB", "en_BB"}, ttt{"BD", "bn_BD"}, ttt{"BE", "nl_BE"}, ttt{"BF", "mos_BF"}, ttt{"BG", "bg_BG"}, ttt{"BH", "ar_BH"}, ttt{"BI", "rn_BI"}, ttt{"BJ", "fr_BJ"}, ttt{"BL", "fr_BL"}, ttt{"BM", "en_BM"}, ttt{"BN", "ms_BN"}, ttt{"BO", "es_BO"}, ttt{"BR", "pt_BR"}, ttt{"BS", "en_BS"}, ttt{"BT", "dz_BT"}, ttt{"BV", "und_BV"}, ttt{"BW", "en_BW"}, ttt{"BY", "be_BY"}, ttt{"BZ", "en_BZ"}, ttt{"CA", "en_CA"}, ttt{"CC", "ms_CC"}, ttt{"CD", "sw_CD"}, ttt{"CF", "fr_CF"}, ttt{"CG", "fr_CG"}, ttt{"CH", "de_CH"}, ttt{"CI", "fr_CI"}, ttt{"CK", "en_CK"}, ttt{"CL", "es_CL"}, ttt{"CM", "fr_CM"}, ttt{"CN", "zh_Hans_CN"}, ttt{"CO", "es_CO"}, ttt{"CR", "es_CR"}, ttt{"CU", "es_CU"}, ttt{"CV", "kea_CV"}, ttt{"CX", "en_CX"}, ttt{"CY", "el_CY"}, ttt{"CZ", "cs_CZ"}, ttt{"DE", "de_DE"}, ttt{"DJ", "aa_DJ"}, ttt{"DK", "da_DK"}, ttt{"DM", "en_DM"}, ttt{"DO", "es_DO"}, ttt{"DZ", "ar_DZ"}, ttt{"EC", "es_EC"}, ttt{"EE", "et_EE"}, ttt{"EG", "ar_EG"}, ttt{"EH", "ar_EH"}, ttt{"ER", "ti_ER"}, ttt{"ES", "es_ES"}, ttt{"ET", "en_ET"}, ttt{"FI", "fi_FI"}, ttt{"FJ", "hi_FJ"}, ttt{"FK", "en_FK"}, ttt{"FM", "chk_FM"}, ttt{"FO", "fo_FO"}, ttt{"FR", "fr_FR"}, ttt{"GA", "fr_GA"}, ttt{"GB", "en_GB"}, ttt{"GD", "en_GD"}, ttt{"GE", "ka_GE"}, ttt{"GF", "fr_GF"}, ttt{"GG", "en_GG"}, ttt{"GH", "ak_GH"}, ttt{"GI", "en_GI"}, ttt{"GL", "iu_GL"}, ttt{"GM", "en_GM"}, ttt{"GN", "fr_GN"}, ttt{"GP", "fr_GP"}, ttt{"GQ", "fan_GQ"}, ttt{"GR", "el_GR"}, ttt{"GS", "und_GS"}, ttt{"GT", "es_GT"}, ttt{"GU", "en_GU"}, ttt{"GW", "pt_GW"}, ttt{"GY", "en_GY"}, ttt{"HK", "zh_Hant_HK"}, ttt{"HM", "und_HM"}, ttt{"HN", "es_HN"}, ttt{"HR", "hr_HR"}, ttt{"HT", "ht_HT"}, ttt{"HU", "hu_HU"}, ttt{"ID", "id_ID"}, ttt{"IE", "en_IE"}, ttt{"IL", "he_IL"}, ttt{"IM", "en_IM"}, ttt{"IN", "hi_IN"}, ttt{"IO", "und_IO"}, ttt{"IQ", "ar_IQ"}, ttt{"IR", "fa_IR"}, ttt{"IS", "is_IS"}, ttt{"IT", "it_IT"}, ttt{"JE", "en_JE"}, ttt{"JM", "en_JM"}, ttt{"JO", "ar_JO"}, ttt{"JP", "ja_JP"}, ttt{"KE", "en_KE"}, ttt{"KG", "ky_Cyrl_KG"}, ttt{"KH", "km_KH"}, ttt{"KI", "en_KI"}, ttt{"KM", "ar_KM"}, ttt{"KN", "en_KN"}, ttt{"KP", "ko_KP"}, ttt{"KR", "ko_KR"}, ttt{"KW", "ar_KW"}, ttt{"KY", "en_KY"}, ttt{"KZ", "ru_KZ"}, ttt{"LA", "lo_LA"}, ttt{"LB", "ar_LB"}, ttt{"LC", "en_LC"}, ttt{"LI", "de_LI"}, ttt{"LK", "si_LK"}, ttt{"LR", "en_LR"}, ttt{"LS", "st_LS"}, ttt{"LT", "lt_LT"}, ttt{"LU", "fr_LU"}, ttt{"LV", "lv_LV"}, ttt{"LY", "ar_LY"}, ttt{"MA", "ar_MA"}, ttt{"MC", "fr_MC"}, ttt{"MD", "ro_MD"}, ttt{"ME", "sr_Latn_ME"}, ttt{"MF", "fr_MF"}, ttt{"MG", "mg_MG"}, ttt{"MH", "mh_MH"}, ttt{"MK", "mk_MK"}, ttt{"ML", "bm_ML"}, ttt{"MM", "my_MM"}, ttt{"MN", "mn_Cyrl_MN"}, ttt{"MO", "zh_Hant_MO"}, ttt{"MP", "en_MP"}, ttt{"MQ", "fr_MQ"}, ttt{"MR", "ar_MR"}, ttt{"MS", "en_MS"}, ttt{"MT", "mt_MT"}, ttt{"MU", "mfe_MU"}, ttt{"MV", "dv_MV"}, ttt{"MW", "ny_MW"}, ttt{"MX", "es_MX"}, ttt{"MY", "ms_MY"}, ttt{"MZ", "pt_MZ"}, ttt{"NA", "kj_NA"}, ttt{"NC", "fr_NC"}, ttt{"NE", "ha_Latn_NE"}, ttt{"NF", "en_NF"}, ttt{"NG", "en_NG"}, ttt{"NI", "es_NI"}, ttt{"NL", "nl_NL"}, ttt{"NO", "nb_NO"}, ttt{"NP", "ne_NP"}, ttt{"NR", "en_NR"}, ttt{"NU", "niu_NU"}, ttt{"NZ", "en_NZ"}, ttt{"OM", "ar_OM"}, ttt{"PA", "es_PA"}, ttt{"PE", "es_PE"}, ttt{"PF", "fr_PF"}, ttt{"PG", "tpi_PG"}, ttt{"PH", "fil_PH"}, ttt{"PK", "ur_PK"}, ttt{"PL", "pl_PL"}, ttt{"PM", "fr_PM"}, ttt{"PN", "en_PN"}, ttt{"PR", "es_PR"}, ttt{"PS", "ar_PS"}, ttt{"PT", "pt_PT"}, ttt{"PW", "pau_PW"}, ttt{"PY", "gn_PY"}, ttt{"QA", "ar_QA"}, ttt{"RE", "fr_RE"}, ttt{"RO", "ro_RO"}, ttt{"RS", "sr_Cyrl_RS"}, ttt{"RU", "ru_RU"}, ttt{"RW", "rw_RW"}, ttt{"SA", "ar_SA"}, ttt{"SB", "en_SB"}, ttt{"SC", "crs_SC"}, ttt{"SD", "ar_SD"}, ttt{"SE", "sv_SE"}, ttt{"SG", "en_SG"}, ttt{"SH", "en_SH"}, ttt{"SI", "sl_SI"}, ttt{"SJ", "nb_SJ"}, ttt{"SK", "sk_SK"}, ttt{"SL", "kri_SL"}, ttt{"SM", "it_SM"}, ttt{"SN", "fr_SN"}, ttt{"SO", "sw_SO"}, ttt{"SR", "srn_SR"}, ttt{"ST", "pt_ST"}, ttt{"SV", "es_SV"}, ttt{"SY", "ar_SY"}, ttt{"SZ", "en_SZ"}, ttt{"TC", "en_TC"}, ttt{"TD", "fr_TD"}, ttt{"TF", "und_TF"}, ttt{"TG", "fr_TG"}, ttt{"TH", "th_TH"}, ttt{"TJ", "tg_Cyrl_TJ"}, ttt{"TK", "tkl_TK"}, ttt{"TL", "pt_TL"}, ttt{"TM", "tk_TM"}, ttt{"TN", "ar_TN"}, ttt{"TO", "to_TO"}, ttt{"TR", "tr_TR"}, ttt{"TT", "en_TT"}, ttt{"TV", "tvl_TV"}, ttt{"TW", "zh_Hant_TW"}, ttt{"TZ", "sw_TZ"}, ttt{"UA", "uk_UA"}, ttt{"UG", "sw_UG"}, ttt{"UM", "en_UM"}, ttt{"US", "en_US"}, ttt{"UY", "es_UY"}, ttt{"UZ", "uz_Cyrl_UZ"}, ttt{"VA", "it_VA"}, ttt{"VC", "en_VC"}, ttt{"VE", "es_VE"}, ttt{"VG", "en_VG"}, ttt{"VI", "en_VI"}, ttt{"VU", "bi_VU"}, ttt{"WF", "wls_WF"}, ttt{"WS", "sm_WS"}, ttt{"YE", "ar_YE"}, ttt{"YT", "swb_YT"}, ttt{"ZA", "en_ZA"}, ttt{"ZM", "en_ZM"}, ttt{"ZW", "sn_ZW"}}
)

func init() {
	DefaultLocale = default_locale
}

func Default() string {
	return DefaultLocale
}

func SetDefault(locale string) (err error) {
	err = VerifyLocale(locale)
	if err == nil {
		DefaultLocale = locale
	}
	return
}

/**
 * Returns the expected locale for a given territory
 */
func ByTerritory(territory string) (string, error) {
	territory = strings.ToUpper(territory)
	for _, v := range dataTerritory {
		if v[0] == territory {
			return v[1], nil
		}
	}
	return DefaultLocale, errors.New("Territory not found, Locale set to default")
}

func Language(locale string) string {
	lang := strings.Split(strings.Trim(locale, "_"), "_")
	return lang[0]
}

// Returns the id of a locale, starting with 1 for the first item
// A '0' return value means not found
// Use id to save RAM
func ID(locale string) (id int) {
	var v string
	for id, v = range dataLocale {
		if v == locale {
			id++
			return
		}
	}
	id = 0
	return
}

// Returns a local by id, "" means not found
func ByID(id int) string {
	if len(dataLocale) < id || id < 1 {
		return ""
	}
	return dataLocale[id-1]
}

func VerifyLocale(locale string) (err error) {
	for _, v := range dataLocale {
		if v == locale {
			return
		}
	}
	err = errors.New("Locale not found")
	return
}
