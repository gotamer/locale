// locale_test
package locale

import (
	"testing"
)

func TestDefault(t *testing.T) {
	w := Default()
	if w != "en_US" {
		t.Errorf("Should be en but is: %s", w)
	}
	err := SetDefault("tr_TR")
	if err != nil {
		t.Errorf(err.Error())
	}
	w = Default()
	if w != "tr_TR" {
		t.Errorf("Should be \"tr_TR\" but is: %s", w)
	}
}

func TestLocale(t *testing.T) {
	r, err := ByTerritory("de")
	if err != nil {
		t.Errorf(err.Error())
	}
	if r != "de_DE" {
		t.Errorf("Should be de_DE but is: %s", r)
	}
	l := Language(r)
	if l != "de" {
		t.Errorf("Should be de but is: %s", l)
		t.Errorf("Should be \"de_DE\" but is: %s", r)
	}
}
